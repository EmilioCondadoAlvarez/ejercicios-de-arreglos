﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicios_de_Arreglos
{
    class pestaña3
    {
        public static void operaciones()
        {
            double a, b, c, d, e, f, g, h, i, j;
            Console.WriteLine("Usaremos Un Arreglo Para Sumar Restar Dividir y Multiplicar Los 10 Numeros Que Ingrese\n");
            Console.WriteLine("Escriba Los 10 Numeros\n");
            a = double.Parse(Console.ReadLine());
            b = double.Parse(Console.ReadLine());
            c = double.Parse(Console.ReadLine());
            d = double.Parse(Console.ReadLine());
            e = double.Parse(Console.ReadLine());
            f = double.Parse(Console.ReadLine());
            g = double.Parse(Console.ReadLine());
            h = double.Parse(Console.ReadLine());
            i = double.Parse(Console.ReadLine());
            j = double.Parse(Console.ReadLine());
            double[] numeros = { b, c, d, e, f, g, h, i, j };
            double suma = 0, resta = 0, multiplicacion = 1, division = a;
            for (int t= 0; t<numeros.Length; t ++)
            {
                suma += numeros[t];
                resta -= numeros[t];
                multiplicacion = (multiplicacion * numeros[t]);
                division = (division / numeros[t]);
            }
            suma = (suma + a);
            resta = (resta - a);
            multiplicacion = (multiplicacion * a);
            Console.WriteLine("La Suma De Todos Los Numeros Es:\t" + suma + "\t\n");
            Console.WriteLine("La Diferencia De Todos Los Numeros Es:\t" + resta + "\t\n");
            Console.WriteLine("El Producto De Todos Los Numeros Es:\t" + multiplicacion + "\t\n");
            Console.WriteLine("El Cociente De Todos Los Numeros Es:\t" + division + "\t\n");
            Console.ReadKey();
        }
            
    }
}
